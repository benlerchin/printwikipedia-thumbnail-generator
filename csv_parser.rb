require 'csv'

class PrintWiki
	class CSVParser
		HEADERS = ['volume', 'name']

		def initialize(csv_path)
			@csv = CSV.read(csv_path, :col_sep => "\t", :headers => HEADERS)
		end
		def each
			@csv.each do |row|
				yield row
			end
		end
	end
end

