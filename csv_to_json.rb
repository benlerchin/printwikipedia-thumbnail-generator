require 'csv'
require 'json'
require_relative 'renderer'

HEADERS = [:volume, :name, :lulu_id]
array = []
CSV.read('books.tsv', :col_sep => "\t", :headers => HEADERS).each do |row|
	array << {name: row[:name], 
						volume: row[:volume],
						lulu_id: row[:lulu_id],
						thumb: "#{PrintWiki::Renderer.filename(row[:name])}.svg"
					}
end
puts array.to_json


