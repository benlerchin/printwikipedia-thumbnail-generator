require 'rvg/rvg'
require 'CGI'
include Magick

RVG::dpi = 300

class PrintWiki
	class Renderer
		C_WIDTH = 432 
		C_HEIGHT = 612 
		C_MARGIN = 30
		TITLE_MAX_LINES = 3
		TOTAL_MAX_LINES = 6
		TITLE_SIZE = 44
		TITLE_HEIGHT = 53
		DEFAULT_WRAP_THRESHOLD = 10
		DEFAULT_PACK_THRESHOLD = 2
		def initialize
			@lines_used = 0
		end

		def build(id, 
							title, 
							prefix = "", 
							pack_threshold = nil,
							wrap_threshold = nil
						 )
			@pack_threshold = pack_threshold || DEFAULT_PACK_THRESHOLD
			@wrap_threshold = wrap_threshold || DEFAULT_WRAP_THRESHOLD
			@filename = "#{Renderer.filename(title)}.svg"
			File.open("thumbs/#{@filename}", 'w+') do |f|
				header(f, C_WIDTH, C_HEIGHT)
				puts "processing #{id}: #{title}"
				titles = split_title(title)
				textbox(f, "\ue02fikipedia", C_WIDTH-C_MARGIN, 85, 70)
				if(prefix != "")
					textbox(f, prefix, C_WIDTH-C_MARGIN, 147, 44)
				end
				textbox(f, "Volume #{id}", C_WIDTH-C_MARGIN, 
									prefix =="" ? 147 : 193, 44)
				from = split_lines(titles[:from], TITLE_SIZE, true)
				to = split_lines(titles[:to], TITLE_SIZE)
				line_c = from.length + to.length

				y = 30 + C_HEIGHT - TITLE_HEIGHT * line_c
				from.each do |l|
					textbox(f, l, C_WIDTH-C_MARGIN, y, TITLE_SIZE)
					y += TITLE_HEIGHT
				end
				to.each do |l|
					textbox(f, l, C_WIDTH-C_MARGIN, y, TITLE_SIZE)
					y += TITLE_HEIGHT
				end

				footer(f)
			end
			@filename
		end

		def textbox(f, text, x, y, size=24, style = "fill:black; font-family:'Linux Libertine O'")
			f.write <<-EOS
<g>
	<text style="font-size: #{size};  text-anchor:end; #{style}" transform="matrix(1 0 0 1 #{x} #{y})">#{text}</text>
</g>
			EOS
		end

		def split_lines(text, size, is_from=false, width=C_WIDTH-C_MARGIN)
			gc = Draw.new do |g|
				g.font_family = "Cardo"
				g.pointsize = size * 1.5
			end
			@lines = []
			@str = ""
			@i = 0
			max_lines = TOTAL_MAX_LINES - @lines_used
			max_lines = TITLE_MAX_LINES if max_lines > TITLE_MAX_LINES

			while @i < text.length
				@str << text[@i]
				@i+= 1
				m = gc.get_type_metrics(@str)
				#if we are nearing width of page
				if m.width > width
					@since = since_space(text)
					wrapped = false
					packed = false
					if @since.length < @wrap_threshold && @since.length > 1
						wrapped = true
						wrap_end()
					end
					@bef = before_space(text)
					if @bef.length < @pack_threshold
						packed = true
						pack_end() 
					end
					if !wrapped && !packed && @str.slice(-1) != " "
						@str << "-"
					end
					@lines << @str 
					@str = ""
				end
			end
			#add remaining chars
			@lines << @str if @str.length > 0
			#add ellipsis and emdash
			last_i = max_lines - 1
			if @lines.length > max_lines && is_from
				@lines = @lines.slice(0, max_lines)
				to_end = -(max_lines + 1)
				@lines[last_i] = @lines[last_i].slice(0..to_end)
				@lines[last_i] << "\u2026 \u2014"
			elsif @lines.length > max_lines
			#shorten and add ellipsis if more than 3 lines
				@lines = @lines.slice(0, max_lines)
				to_end = -(max_lines - 1)
				@lines[last_i] = @lines[last_i].slice(0..to_end)
				@lines[last_i] << "\u2026"
			elsif is_from
				@lines[@lines.length - 1] << " \u2014"
			end
			@lines_used += @lines.length
			@lines
		end

		def wrap_end()
			@str.slice!(-1 * @since.length .. -1)
			@i -= @since.length
		end

		def pack_end()
			@str << @bef
			@i += @bef.length
		end

		def since_space(text, max=10)
			ret = ""
			(0 .. max).each do |p|
				return ret if @i-p < 0 || @i-p > text.length - 1
				unless " " == text[@i-p]
					ret.prepend( text[@i - p] )
				else
					return ret
				end
			end
			ret
		end

		def before_space(text, max=10)
			ret = ""
			(0..max).each do |p|
				return ret if @i+p == text.length
				return ret if " " == text[@i+p]
				ret << text[@i+p] if p >= 0
			end
			return ret
		end

		def near_end?(threshold=3)
			(0..threshold).each do |p|
				return true if @str[@i+p] == " "
			end
			return false
		end


		def header(f, w, h)
			f.write <<-EOS
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
	"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" version="1.1"
			x="0px" y="0px"
		 id="Layer_1"
		 viewBox="0,0,#{C_WIDTH},#{C_HEIGHT}"
		 width="#{C_WIDTH}" height="#{C_HEIGHT}"
		 enable-background="new 0 0 #{C_WIDTH} #{C_HEIGHT}"
		 xml:space="preserve"
		 >
			EOS
		end
		def footer(f)
			f.write "</svg>"
		end

		
		def split_title(title)
			#split at mdash
			title = CGI.escapeHTML(title)
			if title.include? "\u2014"
				split = title.split("\u2014")
			else
				split = title.split('---')
			end
			{from: split[0].strip, to: split[1].strip}
		end

		def self.filename(title)
			title.gsub(/[^A-Za-z0-9]/,'_')
		end
			

	end
end
