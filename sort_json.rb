require 'json'

File.open(ARGV[0], 'r') do |f|
	books = JSON.parse(f.read)
	books.sort! { |x,y| x['volume'].to_i <=> y['volume'].to_i }
	books.each do |b| 
		if b['name'].nil?
			b['name'] = b['title']
		end
		b['volume'] = b['volume'].to_i
		b['sku'] = b['sku'].to_i
		b['lulu_id'] = b['lulu_id'].to_i
	end
	last_volume = books.last['volume'].to_i
	File.open("#{File.basename(ARGV[0], '.json')}_missing.json", 'w+') do |d|
		missing = []
		(1..last_volume).each do |i|
			missing << i unless books.any? {|b| b['volume'].to_i == i }
		end
		JSON.dump missing, d
	end
	File.open("#{File.basename(ARGV[0], '.json')}_sorted.json", 'w+') do |d|
		JSON.dump books, d
	end
end

