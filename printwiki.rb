#!/usr/bin/env ruby

require './csv_parser'
require './json_parser'
require './renderer'

class PrintWiki

	OUTPUT_DIR = "pw_thumbnails"
	CHUNK_SIZE = 200

	def self.help
		puts <<-EOS


PrintWiki SVG Cover Renderer
============================

Accepts a .tsv or .json formatted source file and outputs
book covers in SVG format.

Example TSV row:
0007<tab>Alice Harden <ndash> Andy Bolton

OPTIONAL:
PREFIX:
	text to be shown below "Wikipedia" eg: "Table of Contents"

!!!TODO THE FOLLOWING!!!
PACK_THRESHOLD: 
	max number of chars to move up to next line 
	if it will complete a word on the line it started
WRAP_THRESHOLD
	max number of chars to wrap to next line
	if doing so will allow the world to 
	begin and end on the same line
!!!!!!!!!!!!!!!!!!!!!!!

Usage:
./printwiki.rb entries.tsv [PREFIX] [PACK_THRESHOLD=8] [WRAP_THRESHOLD=4]

		EOS
	end

	def self.format(source, prefix = "", pack_threshold=nil, wrap_threshold=nil )
		raise "No input file provided." if source.nil?	
		ext = File.extname(source)


		if ext == ".csv" || ext == ".tsv"
			parser = PrintWiki::CSVParser.new(source)
		elsif ext == ".json"
			parser = PrintWiki::JSONParser.new(source)
		end


		current_list_i = 0
		last_volume = parser.last['volume']
		lists = []

		new_list = []
		list_len = 100
		
		(1..last_volume.to_i).each do |i|
			if i % list_len == 0 && i > 0
				lists << new_list
				current_list_i += 1
				new_list = []
			end

			row = parser.get.select{|r| r['volume'] == i}[0]
			if row.nil?
				new_row = {volume: i}
				puts new_row
			else
				puts row
				filename = PrintWiki::Renderer.filename(row['name']) + ".svg"
				unless File.exist?("thumbs/#{filename}")
					puts "#{filename} does not exist. building..."
					filename = PrintWiki::Renderer.new.build(row['volume'].to_i.to_s, 
													row['name'], 
													prefix,
													pack_threshold && pack_threshold.to_i,
													wrap_threshold && wrap_threshold.to_i
															 )
				#why we have to use slow --with-gui option:
				#http://stackoverflow.com/questions/8422558/converting-text-into-path-svg-on-server
				`inkscape -f thumbs/#{filename} --with-gui --verb EditSelectAll --verb ObjectToPath --verb FileSave --verb FileQuit`
				end
				new_row = {
					name: row['name'],
					volume: row['volume'],
					sku: row['sku'],
					lulu_id: row['lulu_id'],
					thumb: filename
				}
				#new_row['thumb'] = filename
			end
			new_list << new_row
		end

		lists << new_list

		(0..current_list_i).each do |i|
			File.open dest(source, ext, i), 'w+' do |d|
				JSON.dump lists[i], d
			end
		end
	end

	def self.dest(source, ext, num)
		dest = File.basename(source, ext) + "_web_#{num}.json"
	end
end

if ARGV[0].nil? || ARGV[0].include?("-h") || ARGV[0].include?("--help")
	PrintWiki.help
else
	PrintWiki.format(*ARGV)
end
