require 'json'

class PrintWiki
	class JSONParser

		def initialize(json_path)
			File.open(json_path, 'r') do |f|
				@json = JSON.parse(f.read)
			end
		end
		def each
			@json.each do |row|
				yield row
			end
		end
		def last
			@json.last
		end
		def get
			@json
		end
	end
end
