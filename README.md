PrintWiki SVG Cover Renderer
============================

Accepts a .tsv or .json formatted source file and outputs
book covers in SVG format.

Example TSV row:
0007<tab>Alice Harden <ndash> Andy Bolton

OPTIONAL:
PREFIX:
	text to be shown below "Wikipedia" eg: "Table of Contents"

TODO THE FOLLOWING:
PACK_THRESHOLD: 
	max number of chars to move up to next line 
	if it will complete a word on the line it started
WRAP_THRESHOLD
	max number of chars to wrap to next line
	if doing so will allow the world to 
	begin and end on the same line

Usage:
```
./printwiki.rb entries.tsv [PREFIX] [PACK_THRESHOLD=8] [WRAP_THRESHOLD=4]
```

PrintWiki Sort JSON
==================
Takes a JSON list of volumes, and outputs a list sorted by volume number.
Saves sorted list with \_sorted.json suffix. Lists missing entries with
\_missing.json suffix.

Usage:
```
ruby sort_json.rb path/to/volumes_list.json
```

PrintWiki Preprocess CSV
=======================
Handles CSV files obtained from Lulu. Sorts, de-dupes, and converts to
json.

see example.csv for input format

Usage:
```
ruby preprocessing/preprocess_csv.rb path/to/example.csv
```
