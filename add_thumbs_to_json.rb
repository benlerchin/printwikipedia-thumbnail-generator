#!/usr/bin/env ruby

require 'json'
require_relative 'renderer'

def process()
	File.open('books_dump.json', 'r') do |source|
		books = JSON.parse(source.read)
		books.each do |book|
			book['thumb'] = PrintWiki::Renderer.filename(book['name'])
		end
		File.open('books.json', 'w+') do |dest|
			dest.write JSON.generate(books)
		end
	end
end


process(*ARGV)
