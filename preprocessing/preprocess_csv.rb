require 'csv'
require 'json'


File.open(ARGV[0], 'r') do |f|
	csv = CSV.parse(f.read, headers: :first_row)
	json = [];
	csv.each do |r| 
		next unless r['status'].include? "available"
		parse = /^(.*?)[: ]*?Volume ([0-9]*), (.*)$/.match(r['title'])

		json << {	sku: r['product_id'].to_i, 
							lulu_id: r['con_id'].to_i, 
							volume: parse[2].to_i,
							title: parse[3]
		}
	end
	volumes = []
	#sort by lulu_id == order of creation so we can delete older
	#of duplicates
	json.sort! { |x,y| x[:lulu_id] <=> y[:lulu_id] }
	json.reverse_each do |r|
		if (volumes.include? r[:volume])
			json.delete(r)
		else
			volumes.push(r[:volume])
		end
	end
	#now sort by what we actually want
	json.sort! { |x,y| x[:volume] <=> y[:volume] }
	File.open("#{File.basename(ARGV[0], '.csv')}_sorted.json", 'w+') do |d|
		JSON.dump json, d
	end
end
